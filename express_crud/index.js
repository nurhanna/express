import express from "express";
import fs from "fs";
import { v4 as uuidv4 } from "uuid";

const app = express();

const port = 3000;

app.use(express.urlencoded({ extended: true }));

const data = JSON.parse(fs.readFileSync("./database/data.json"));
const country = data.country;

// Read data from database
app.get("/country", (req, res) => {
  try {
    // console.log(data)
    res.json({
      status: "success",
      statusCode: 200,
      message: "Data fetched successfully",
      data: country,
    });
  } catch (err) {
    res.json({
      status: "error",
      statusCode: 404,
      message: "Not Found",
    });
  }
});

//  get details of country
app.get("/country/:id", (req, res) => {
  try {
    const matchingCountry = country.find((item) => item.id == req.params.id);

    if (matchingCountry) {
      res.json({
        status: "Success",
        statusCode: 200,
        message: "Success",
        data: matchingCountry,
      });
    } else {
      res.json({
        status: "error",
        statusCode: 404,
        message: "Not Found",
      });
    }
  } catch (err) {
    res.json({
      status: "error",
      statusCode: 404,
      message: "Not Found",
    });
  }
});

//   Create a new country
app.post("/country", (req, res) => {
  try {
    const { name, capital } = req.body;

    if (!name) {
      return res.status(400).json({
        status: "error",
        statusCode: 400,
        message: "Name is required",
      });
    }

    // Check for duplicate country name
    const duplicateName = data.country.some((country) => country.name === name);
    if (duplicateName) {
      return res.status(400).json({
        status: "error",
        statusCode: 400,
        message: "Country name already exists",
      });
    }

    const newCountry = {
      id: uuidv4(),
      name: name,
      capital: capital || "",
    };

    data.country.push(newCountry);

    // Update country in database asynchronously
    fs.writeFile(
      "./database/data.json",
      JSON.stringify(data, null, 2),
      "utf-8",
      (err) => {
        if (err) {
          return res.status(500).json({
            status: "error",
            statusCode: 500,
            message: "Internal server error",
          });
        }

        res.status(201).json({
          status: "success",
          statusCode: 201,
          message: "Created successfully",
          data: newCountry,
        });
      }
    );
  } catch (err) {
    res.status(500).json({
      status: "error",
      statusCode: 500,
      message: "Internal server error",
    });
  }
});

//  update country based on id
app.put("/country/:id", (req, res) => {
  try {
    const { id } = req.params;
    const { name, capital } = req.body;

    const foundCountry = country.find((item) => {
      item.id === id;
      return item;
    });
    // console.log(foundCountry);

    if (foundCountry) {
      // Check for duplicate name
      const duplicateName = country.some(
        (item) => item.id !== id && item.name === name
      );
      if (duplicateName) {
        return res.status(400).json({
          status: "error",
          statusCode: 400,
          message: "Country name already exists",
        });
      }

      foundCountry.name = name;
      foundCountry.capital =
        capital !== undefined ? capital : foundCountry.capital;

      fs.writeFile(
        "./database/data.json",
        JSON.stringify(data, null, 2),
        "utf-8",
        (err) => {
          if (err) {
            return res.status(500).json({
              message: "Internal server error",
            });
          }

          // console.log("successfully");
          res.sendStatus(200);
        }
      );
      res.status(201).json({
        status: "success",
        statusCode: 200,
        message: "Updated successfully",
        data: foundCountry,
      });
    } else {
      res.status(404).json({
        status: "error",
        statusCode: 404,
        message: "Not Found",
      });
    }
  } catch (err) {
    console.log(err);
    res.status(400).json({
      status: "error",
      statusCode: 400,
      message: "Bad Request",
    });
  }
});

// delete the country
app.delete("/country/:id", (req, res) => {
  try {
    const { id } = req.params;

    const indexToDelete = country.findIndex((item) => {
      item.id === id;
      return item;
    });

    country.splice(indexToDelete, 1);

    // Update country data in the database asynchronously
    fs.writeFile(
      "./database/data.json",
      JSON.stringify(data, null, 2),
      "utf-8",
      (err) => {
        if (err) {
          return res.status(500).json({
            status: "error",
            statusCode: 500,
            message: "Internal server error",
          });
        }

        // console.log("Delete successfully");
        res.status(200).json({
          status: "success",
          statusCode: 200,
          message: "Country deleted successfully",
        });
      }
    );
  } catch (err) {
    console.log(err);
    res.status(400).json({
      status: "error",
      statusCode: 400,
      message: "Bad Request",
    });
  }
});

app.all("*", (req, res) => {
  res.status(404).json({
    message: "Api not found",
  });
});

app.listen(port, () => {
  console.log(`My app on port ${port}`);
});
