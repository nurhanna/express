import express from "express";
import pokemonRouter from "./Routes/pokemonRouter.js";

const app = express();

const port = 3000;

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use("/api/pokemon", pokemonRouter);

app.listen(port, () => {
  console.log(`My app pokemon on port ${port}`);
});

export default app;
