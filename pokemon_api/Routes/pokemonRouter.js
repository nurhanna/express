import express from "express";
import pokemonController from "../Controllers/pokemonController.js";

const pokemonRouter = express.Router();

pokemonRouter.get("/", pokemonController.getPokemon);
pokemonRouter.get("/detail/:name", pokemonController.getPokemonDetail);
pokemonRouter.get("/catch", pokemonController.catchPokemon);
pokemonRouter.get("/win", pokemonController.winPokemon);
pokemonRouter.get("/release", pokemonController.releasePokemon);
pokemonRouter.get("/rename/:id", pokemonController.renamePokemon);

export default pokemonRouter;
