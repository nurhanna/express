import axios from "axios";
import fs from "fs";
import { v4 as uuidv4 } from "uuid";
import pokemonController from "../Controllers/pokemonController";

jest.mock("axios");
jest.mock("fs");

describe("API Test", () => {
  beforeEach(() => {
    jest.clearAllMocks(); // Clear all mock calls before each test
  });

  test("GET /api/pokemon should return statusCode 200 and have expectedPokemons", async () => {
    const expectedResponse = {
      status: "success",
      statusCode: 200,
      data: [
        {
          name: "bulbasaur",
          url: "https://pokeapi.co/api/v2/pokemon/1/",
        },
      ],
    };

    // Mock axios.get to resolve with the expectedResponse data
    axios.get.mockResolvedValue({ data: { results: expectedResponse.data } });
    const req = {};
    const res = {
      json: jest.fn(),
    };

    await pokemonController.getPokemon(req, res);

    expect(axios.get).toBeCalledWith("https://pokeapi.co/api/v2/pokemon/");

    expect(res.json).toBeCalledWith(expectedResponse);
  });

  // test get pokemon failed
  test("GET /api/pokemon should return error response on failure", async () => {
    // Mock axios.get to simulate a failure
    const errorMessage = "Network error";
    axios.get.mockRejectedValue(new Error(errorMessage));

    const req = {};
    const res = {
      json: jest.fn(),
    };

    await pokemonController.getPokemon(req, res);

    expect(axios.get).toBeCalledWith("https://pokeapi.co/api/v2/pokemon/");

    expect(res.json).toBeCalledWith({
      status: "error",
      statusCode: 500,
      message: "Internal server error",
    });
  });

  //detail success testing
  test("GET /api/pokemon/:name should return success response with Pokemon detail", async () => {
    // Mock axios.get to simulate a successful response
    const mockResponse = {
      data: {
        effect_changes: [],
        effect_entries: [],
        flavor_text_entries: [],
        generation: {
          name: "generation-iii",
          url: "https://pokeapi.co/api/v2/generation/3/",
        },
        id: 65,
        is_main_series: true,
        name: "overgrow",
      },
    };
    axios.get.mockResolvedValue(mockResponse);

    const req = {
      params: {
        name: "overgrow",
      },
    };
    const res = {
      json: jest.fn(),
    };

    await pokemonController.getPokemonDetail(req, res);

    expect(axios.get).toBeCalledWith(
      "https://pokeapi.co/api/v2/pokemon/overgrow"
    );

    expect(res.json).toBeCalledWith({
      status: "succes",
      statusCode: 200,
      data: mockResponse.data,
    });
  });

  test("GET /api/pokemon/:name should return error response on failure", async () => {
    axios.get.mockRejectedValue(new Error("Network error"));

    const req = {
      params: {
        name: "nonexistent-pokemon",
      },
    };
    const res = {
      status: jest.fn().mockReturnThis(),
      json: jest.fn(),
    };

    await pokemonController.getPokemonDetail(req, res);

    expect(axios.get).toBeCalledWith(
      "https://pokeapi.co/api/v2/pokemon/nonexistent-pokemon"
    );

    expect(res.status).toBeCalledWith(400);
    expect(res.json).toBeCalledWith({
      status: "error",
      statusCode: 400,
      message: "Bad Request",
    });
  });
  // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++=
  // test for catch pokemon

  test("should return success response with 'winner' result", async () => {
    fs.readFileSync = jest.fn().mockReturnValue(
      JSON.stringify({
        winPokemon: [],
      })
    );

    axios.get.mockResolvedValue({
      data: {
        results: [{ name: "Pikachu" }, { name: "Charmander" }],
      },
    });

    Math.random = jest.fn().mockReturnValue(0.4);

    fs.writeFileSync = jest.fn();

    const expectedResponse = {
      status: "success",
      statusCode: 200,
      result: "winner",
      data: { name: "Pikachu" },
    };

    const req = {};
    const res = {
      json: jest.fn(),
    };

    await pokemonController.catchPokemon(req, res);

    expect(res.json).toHaveBeenCalledWith(expectedResponse);
    expect(fs.writeFileSync).toHaveBeenCalled();

    Math.random.mockRestore();
  });

  // loose

  test("should return success response with 'Loose' result", async () => {
    fs.readFileSync = jest.fn().mockReturnValue(
      JSON.stringify({
        winPokemon: [],
      })
    );
    axios.get.mockResolvedValue({
      data: {
        results: [{ name: "Pikachu" }, { name: "Charmander" }],
      },
    });

    // Mock Math.random to make it return a value greater than 0.5
    Math.random = jest.fn().mockReturnValue(0.6);

    const expectedResponse = {
      status: "success",
      statusCode: 200,
      result: "Loose",
    };

    const req = {};
    const res = {
      json: jest.fn(),
    };

    await pokemonController.catchPokemon(req, res);

    expect(res.json).toHaveBeenCalledWith(expectedResponse);

    Math.random.mockRestore();
  });

  // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  // failed
  test("should return error response on failure", async () => {
    // Mock Math.random to throw an error
    Math.random = jest.fn(() => {
      throw new Error("Random number generation error");
    });

    const expectedResponse = {
      status: "failed",
      statusCode: 500,
      result: "Internal Server Error",
    };

    const req = {};
    const res = {
      json: jest.fn(),
    };

    await pokemonController.catchPokemon(req, res);

    expect(res.json).toHaveBeenCalledWith(expectedResponse);

    Math.random.mockRestore();
  });

  // Test for winPokemon
  test("GET /api/win-pokemon should return success response with winPokemon data", async () => {
    const mockWinPokemonData = [
      {
        id: "7bcb9255-c99b-4991-bafb-66cf40840d2d",
        name: "blastoise-21",
        release: 9,
      },
      {
        id: "9d86998f-1f65-4f97-b872-f5699ef785ef",
        name: "charmander",
        release: 0,
      },
    ];

    // Mock fs.readFileSync to return the mockWinPokemonData
    jest.spyOn(fs, "readFileSync").mockReturnValue(
      JSON.stringify({
        winPokemon: mockWinPokemonData,
      })
    );

    const req = {};
    const res = {
      json: jest.fn(),
    };

    await pokemonController.winPokemon(req, res);

    expect(res.json).toBeCalledWith({
      status: "success",
      statusCode: 200,
      data: mockWinPokemonData,
    });

    expect(fs.readFileSync).toHaveBeenCalledWith("./Database/pokemon.json");
  });

  // Test for failed winPokemon
  test("GET /api/win-pokemon should return error response if cant read winPokemon data", async () => {
    jest.spyOn(fs, "readFileSync").mockImplementation(() => {
      throw new Error("Unable to read data");
    });

    const req = {};
    const res = {
      json: jest.fn(),
    };

    await pokemonController.winPokemon(req, res);

    expect(res.json).toBeCalledWith({
      status: "failed",
      statusCode: 500,
      message: "Internal Server Error",
    });

    expect(fs.readFileSync).toHaveBeenCalledWith("./Database/pokemon.json");
  });

  // release
  test("GET /api/release-pokemon should return success response with prime number check", async () => {
    const originalMathRandom = Math.random;
    Math.random = jest.fn().mockReturnValue(0.5);

    const req = {};
    const res = {
      json: jest.fn(),
    };

    await pokemonController.releasePokemon(req, res);

    Math.random = originalMathRandom;

    expect(res.json).toBeCalledWith({
      status: "success",
      statusCode: 200,
      isPrime: true,
    });
  });

  test("GET /api/release-pokemon should return error response on failure", async () => {
    // Mock Math.random to throw an error
    Math.random = jest.fn(() => {
      throw new Error("Random number generation error");
    });

    const expectedResponse = {
      status: "failed",
      statusCode: 500,
      message: "Internal Server Error",
    };

    const req = {};

    const res = {
      json: jest.fn(),
    };

    await pokemonController.releasePokemon(req, res);

    expect(res.json).toHaveBeenCalledWith(expectedResponse);

    Math.random.mockRestore();
  });

  // rename
  test("should rename the Pokemon and return success response", async () => {
    const data = {
      winPokemon: [
        {
          id: "1",
          name: "Pikachu",
          release: 0,
        },
      ],
    };

    fs.readFileSync.mockReturnValue(JSON.stringify(data));
    fs.writeFileSync.mockImplementation(() => {});

    const req = {
      params: { id: "1" },
    };
    const res = {
      json: jest.fn(),
    };

    await pokemonController.renamePokemon(req, res);

    expect(res.json).toHaveBeenCalledWith({
      status: "success",
      statusCode: 200,
      data: {
        id: "1",
        name: "Pikachu-0",
        release: 1,
      },
    });
  });

  test("should return failed response when Win Pokemon is not found", async () => {
    const data = {
      winPokemon: [],
    };

    fs.readFileSync.mockReturnValue(JSON.stringify(data));

    const req = {
      params: { id: "1" },
    };
    const res = {
      json: jest.fn(),
    };

    await pokemonController.renamePokemon(req, res);

    expect(res.json).toHaveBeenCalledWith({
      status: "failed",
      statusCode: 404,
      message: "Pokemon not found",
    });
  });
});
