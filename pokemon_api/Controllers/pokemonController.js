import axios from "axios";
import fs from "fs";
import { v4 as uuidv4 } from "uuid";

// const getPokemonData = async () => {
//   const url = "https://pokeapi.co/api/v2/pokemon/";
//   try {
//     const response = await axios.get(url);
//     const data = response.data.results;
//     return data;
//   } catch (err) {
//     // console.log(err);
//     throw err;
//   }
// };

// // function for check prime'
function isPrime(num) {
  if (num <= 1) {
    return false;
  }
  for (let i = 2; i <= num / 2; i++) {
    if (num % i == 0) return false;
  }
  return true;
}

// check if fibonacci
const fibonacci = (n) => {
  if (n === 0) return 0;
  if (n === 1) return 1;

  let prev = 0;
  let current = 1;

  for (let i = 2; i <= n; i++) {
    const next = prev + current;
    prev = current;
    current = next;
  }

  return current;
};

const pokemonController = {
  getPokemon: async (req, res) => {
    const url = "https://pokeapi.co/api/v2/pokemon/";
    try {
      const response = await axios.get(url);
      res.json({
        status: "success",
        statusCode: 200,
        data: response.data.results,
      });
    } catch (err) {
      res.json({
        status: "error",
        statusCode: 500,
        message: "Internal server error",
      });
    }
  },

  getPokemonDetail: async (req, res) => {
    try {
      const url = `https://pokeapi.co/api/v2/pokemon/${req.params.name}`;
      const response = await axios.get(url);

      res.json({
        status: "succes",
        statusCode: 200,
        data: response.data,
      });
    } catch (err) {
      res.status(400).json({
        status: "error",
        statusCode: 400,
        message: "Bad Request",
      });
    }
  },

  catchPokemon: async (req, res) => {
    try {
      const data = JSON.parse(fs.readFileSync("./Database/pokemon.json"));

      // const dataPokemons = await getPokemonData();
      const url = "https://pokeapi.co/api/v2/pokemon/";
      const response = await axios.get(url);
      const dataPokemons = response.data.results;
      //   console.log("data pokemon", dataPokemons);
      const successProbability = Math.random() <= 0.5;
      if (successProbability) {
        const random = Math.floor(Math.random() * dataPokemons.length);
        const myPokemon = {
          id: uuidv4(),
          name: dataPokemons[random].name,
          release: 0,
        };
        // console.log(dataPokemons[random].name);
        data.winPokemon.push(myPokemon);
        fs.writeFileSync(
          "./Database/pokemon.json",
          JSON.stringify(data, null, 2),
          "utf-8"
        );
        res.json({
          status: "success",
          statusCode: 200,
          result: "winner",
          data: dataPokemons[random],
        });
      } else {
        res.json({
          status: "success",
          statusCode: 200,
          result: "Loose",
        });
      }
    } catch (err) {
      res.json({
        status: "failed",
        statusCode: 500,
        result: "Internal Server Error",
      });
    }
  },

  winPokemon: async (req, res) => {
    try {
      const data = JSON.parse(fs.readFileSync("./Database/pokemon.json"));
      res.json({
        status: "success",
        statusCode: 200,
        data: data.winPokemon,
      });
    } catch (err) {
      res.json({
        status: "failed",
        statusCode: 500,
        message: "Internal Server Error",
      });
    }
  },

  releasePokemon: async (req, res) => {
    try {
      const random = Math.floor(Math.random() * 20) + 1;
      // const dataPokemons = await getPokemonData();
      res.json({
        status: "success",
        statusCode: 200,
        isPrime: isPrime(random),
      });
    } catch (err) {
      res.json({
        status: "failed",
        statusCode: 500,
        message: "Internal Server Error",
      });
    }
  },

  renamePokemon: async (req, res) => {
    const data = JSON.parse(fs.readFileSync("./Database/pokemon.json"));
    const id = req.params.id;

    const dataPokemon = data.winPokemon.find((pokemon) => pokemon.id === id);
    if (!dataPokemon) {
      res.json({
        status: "failed",
        statusCode: 404,
        message: "Pokemon not found",
      });
      return;
    }

    const splitName = dataPokemon.name.split("-");
    const firstName = splitName[0];
    const release = dataPokemon.release;
    const fibonacciNumber = fibonacci(release);
    const finalName = firstName + "-" + fibonacciNumber;

    // update db
    dataPokemon.name = finalName;
    dataPokemon.release = release + 1;
    fs.writeFileSync(
      "./Database/pokemon.json",
      JSON.stringify(data, null, 2),
      "utf-8"
    );

    res.json({
      status: "success",
      statusCode: 200,
      data: dataPokemon,
    });
  },
};

export default pokemonController;
